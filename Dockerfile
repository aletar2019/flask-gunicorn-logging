FROM python:slim

RUN mkdir /home/app
COPY ./Pipfile* /home/app/

RUN python3 -m pip install --upgrade pip setuptools wheel pipenv && \
    cd /home/app && pipenv install --system --deploy

ENV FLASK_ENV=production
ENV FLASK_APP=app:app

COPY . /home/app

WORKDIR /home/app

CMD ["gunicorn"]