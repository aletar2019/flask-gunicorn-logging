import os

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'console_prod': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'gunicorn': {
            'handlers': ['console' if os.getenv("FLASK_ENV", None) == 'development' else 'console_prod'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'app': {
            'handlers': ['console' if os.getenv("FLASK_ENV", None) == 'development' else 'console_prod'],
            'level': 'INFO',
            'propagate': True,
        }
    }
}
