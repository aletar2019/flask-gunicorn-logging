import logging

from logging import config

from flask import Flask

from logconfig import LOGGING

app = Flask(__name__)


config.dictConfig(LOGGING)

log = logging.getLogger("app")


@app.route('/')
def hello_world():
    log.error("Index 'hello_world'")
    return 'Hello Sammy!'
