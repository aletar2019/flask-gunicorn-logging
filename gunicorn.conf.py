import multiprocessing

from logconfig import LOGGING

wsgi_app = "app:app"
bind = "0.0.0.0:5000"
workers = multiprocessing.cpu_count() * 2 + 1
logconfig_dict = LOGGING